package unal.estocasticos;

import java.util.Random;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.GammaDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.PoissonDistribution;

public class Punto5 {

	public enum Distribucion {
		Normal, Exponential, Gamma, ChiSquared, Poisson
	}

	public static int[] numeros = new int[] { 5, 7, 1, 6, 8, 4 };
	public static Random rand = new Random();
	public static long timeIni, timeEnd;

	public static void bubbleSort(double[] arr) {
		boolean swapped = true;
		int j = 0;
		double tmp;
		while (swapped) {
			swapped = false;
			j++;
			for (int i = 0; i < arr.length - j; i++) {
				if (arr[i] > arr[i + 1]) {
					tmp = arr[i];
					arr[i] = arr[i + 1];
					arr[i + 1] = tmp;
					swapped = true;
				}
			}
		}
	}

	public static void quicksort(double A[], int izq, int der) {
		double pivote = A[izq];
		int i = izq;
		int j = der;
		double aux;

		while (i < j) {
			while (A[i] <= pivote && i < j)
				i++;
			while (A[j] > pivote)
				j--;
			if (i < j) {
				aux = A[i];
				A[i] = A[j];
				A[j] = aux;
			}
		}
		A[izq] = A[j];
		A[j] = pivote;
		if (izq < j - 1)
			quicksort(A, izq, j - 1);
		if (j + 1 < der)
			quicksort(A, j + 1, der);
	}

	public static double[] generarLista(Distribucion distribucion) {
		double[] resultado = new double[14000];
		switch (distribucion) {
		case Normal:
			for (int i = 0; i < resultado.length; i++) {
				resultado[i] = new NormalDistribution().sample();
			}
			break;
		case Exponential:
			for (int i = 0; i < resultado.length; i++) {
				resultado[i] = new ExponentialDistribution(1).sample();
			}
			break;
		case Gamma:
			for (int i = 0; i < resultado.length; i++) {
				resultado[i] = new GammaDistribution(5, 1.0).sample();
			}
			break;
		case ChiSquared:
			for (int i = 0; i < resultado.length; i++) {
				resultado[i] = new ChiSquaredDistribution(4).sample();
			}
			break;
		case Poisson:
			for (int i = 0; i < resultado.length; i++) {
				resultado[i] = new PoissonDistribution(4).sample();
			}
			break;
		default:
			break;
		}
		return resultado;
	}

	public static String arrayToString(double[] arreglo) {
		String cadena = "";
		for (double numero : arreglo) {
			cadena = cadena + numero + ", ";
		}
		return cadena;
	}

	public static void main(String[] args) {
		double[] datos = null;
		Distribucion[] distribuciones = Distribucion.values();
		System.out.println("Parcial 1 ME (Punto 5): Hugo Alejandro Arias, Julio Cesar Sanchez\n");
		for (int x = 1; x <= 10; x++) {
			System.out.println("Ejecucion: "+x);
			for (int i = 0; i < distribuciones.length; i++) {
				datos = generarLista(distribuciones[i]);
				System.out.println("Distribucion: " + distribuciones[i].toString());

				System.out.print("*BubbleSort: ");
				timeIni = System.currentTimeMillis();
				bubbleSort(datos);
				timeEnd = System.currentTimeMillis() - timeIni;
				System.out.println(timeEnd + " milisegundos");

				System.out.print("*Quicksort: ");
				timeIni = System.currentTimeMillis();
				quicksort(datos, 0, datos.length - 1);
				timeEnd = System.currentTimeMillis() - timeIni;
				System.out.println(timeEnd + " milisegundos");
			}
		}
	}

}
